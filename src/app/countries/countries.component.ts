import { Component, OnInit } from '@angular/core';
import { Country } from '../model/country';
import { CountryService } from '../services/country.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {

  countries:Array<Country> = [];

  constructor(
    private countryServ: CountryService
  ) { }

  ngOnInit() {
    this.getCountries();
  }

  getCountries():void {
    this.countryServ.getCountries().subscribe(
      countries => this.countries = countries
    );
  }

}
