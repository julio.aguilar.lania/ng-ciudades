import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Country } from '../model/country';

import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CountryService {

    resourceUrl = environment.baseUrl + '/countries';

    constructor(
        private http: HttpClient
    ) { }

    getCountries(): Observable<Country[]> {
        return this.http.get<Country[]>(this.resourceUrl);
    }

    getCountry(code): Observable<Country> {
        return this.http.get<Country>(this.resourceUrl + '/' + code);
    }

}
