import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

import { CountryService } from './country.service';

describe('CountryService', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [CountryService],
            imports: [HttpClientTestingModule]
        });
    });

    beforeEach(() => {
        httpClient = TestBed.get(HttpClient);
        httpTestingController = TestBed.get(HttpTestingController);
    });

    it('should be created', inject([CountryService], (service: CountryService) => {
        expect(service).toBeTruthy();
    }));
});
